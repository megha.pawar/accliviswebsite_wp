<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'accliviswp' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'W*F(<A2u1`3Se+ga4@uIm<$,eW@mGK6y50cEDH&Sc+{ONx7kNJ[5.M[6E4;RnzT}' );
define( 'SECURE_AUTH_KEY',  '!-_eCF(L|/~`~GZa/nR@0OJ74-oc;X7qv7)m%3+*}^L{tx40Q[ aY?rIej27]-LV' );
define( 'LOGGED_IN_KEY',    '1`YU: nOCc;rPH):|G<@jp2jnQj<YGMM}|w!Pto3g%D1Mhe%xTRSe^~`vqi,?P9:' );
define( 'NONCE_KEY',        'KSlEO66d5 ]f{/ATF&~4Y*edCY%1@Q+l[v{_)w?Hl1=&o=+]kA=l33I*:pbo|? m' );
define( 'AUTH_SALT',        'xF`cRR<COu&j:ZO2o)kCrA@DS#b[L{X<;$ncW|_kKLHgkz-sj|ei?vOfRI-y-T)O' );
define( 'SECURE_AUTH_SALT', 'dkkeoOBfPoI2OK|}FS-q&ed%gMHP3*Qjc]/s@]q&^EY1olGoT_=EP::T(.AP+wWk' );
define( 'LOGGED_IN_SALT',   '!<sYSYq<zr,U!=4H[dOctN;M5(M?c>CuN?-G5bK,(q@*VMcA9uE1u/oY-4_?#=N?' );
define( 'NONCE_SALT',       'i<hiD)[4;W@75GBdi2Tw#ppFFBch=nlO]iH|$MFpPe9}Q]#U~p~|c$.IK`^h<pK$' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';


// username: Megha
// password: Acclivis@Megha@123
//site title: Acclivis Technologies
//email: megha.pawar@acclivistechnologies.com